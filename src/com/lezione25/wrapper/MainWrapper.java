package com.lezione25.wrapper;

public class MainWrapper {

	public static void main(String[] args) {

//		int a = 5;
//		System.out.println(a);
//		
		//Utilizzo della classe Wrapper Integer
//		Integer b = new Integer(5);
//		String c = b.toString();
//		System.out.println(c);
		
		//ESEMPIO DI BOXING (inscatolo in un oggetto un tipo primitivo);
		Integer var_1 = new Integer(5);
		Double var_2 = new Double(2.5d);
		
		float valore_primitivo = 5.6f;
		Float var_3 = new Float(valore_primitivo);
		
		//ESEMPIO DI AUTOBOXING
		Integer var_4 = 98;
		Double var_5 = 8.5d;
		Float var_6 = 5.6f;
		
		Double var_7 = Double.parseDouble(var_6.toString());	//PORCAROUND! ;)
		
		//ESEMPIO DI UNBOXING (passo dal tipo oggetto al tipo primitivo)
		int var_8 = var_4;
		double var_9 = var_5;
		double var_10 = var_6;		//Torno a poter utilizzare il riversamento!
	}

}
