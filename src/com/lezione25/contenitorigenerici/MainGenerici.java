package com.lezione25.contenitorigenerici;

import java.util.ArrayList;
import java.util.List;

public class MainGenerici {
	
	public static void stampa(String varInput) {
		System.out.println(varInput);
	}

	public static void main(String[] args) {

//		List<String> elenco_auto = new ArrayList<String>();
//		elenco_auto.add("BMW");
//		elenco_auto.add("FIAT");
//		elenco_auto.add("Lamborghini");
//		elenco_auto.add("Bugatti");
//		
//		for(String i: elenco_auto) {
//			System.out.println(i);
//		}
//		
//		elenco_auto.forEach((i) -> {System.out.println(i);});
		
		//////////////////////////////////////////////
		
		List elenco_generico = new ArrayList();
		elenco_generico.add("BMW");
		elenco_generico.add("Lamborghini");
		elenco_generico.add(5);
		elenco_generico.add(5.5f);
		elenco_generico.add(new Studente("Giovanni"));
		
		elenco_generico.forEach((i) -> {
			System.out.println(i);
		});
		elenco_generico.forEach((i) -> {
			stampa(i.toString());
		});
//		for(String i: elenco_generico) {	!!!NONONONONO FOR NON PERMESSO A CAUSA DEL TIPO STRING OBBLIGATO :(
//			System.out.println(i);
//		}
		
//		Studente gio = new Studente("Giovanni");
//		System.out.println(gio);		//Se non definisco il tostring nella classe mi restituisce l'indirizzo di memoria, altrimenti i dettagli che gli specifico! ;)
		
	}

}
