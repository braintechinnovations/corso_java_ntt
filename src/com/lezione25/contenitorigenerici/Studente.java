package com.lezione25.contenitorigenerici;

public class Studente {
	private String nome;

	public Studente(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "Studente [nome=" + nome + "]";
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
}
