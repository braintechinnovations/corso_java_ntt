package com.lezione14.oopsemplice.esempio;

public class Studente {

	private String nome;
	private String cognome;
	private String matricola;
	
	Studente(){
		System.out.println("Ho creato uno studente");
	}
	
	Studente(String var_nome, String var_cognome){
		this.nome = var_nome;
		this.cognome = var_cognome;
		this.matricola = "Non Definita!";
	}
	
	/**
	 * 
	 * @param var_nome
	 * @param var_cognome
	 * @param var_matricola in ingresso ha il formato String "123456" ma deve essere salvata con l'anno di riferimento "2021-123456"
	 */
	Studente(String var_nome, String var_cognome, String var_matricola){
		this.nome = var_nome;
		this.cognome = var_cognome;
		this.matricola = this.formattaMatricola(var_matricola);
	}
	
	public void setNome(String var_nome) {
		this.nome = var_nome;
	}
	public void setCognome(String var_cognome) {
		this.cognome = var_cognome;
	}
	public void setMatricola(String var_matricola) {
		this.matricola = this.formattaMatricola(var_matricola);
	}
	
	private String formattaMatricola(String var_matricola) {
		return "2021-" + var_matricola;
	}
	
	public String getNome() {
		return this.nome;
	}
	public String getCognome() {
		return this.cognome;
	}
	public String getMatricola() {
		return this.matricola;
	}
	
	public String stampaStudente() {
		String frase = "";
		frase += "Nome: " + this.nome + "\n";
		frase += "Cognome: " + this.cognome + "\n";
		frase += "Matricola: " + this.matricola + "\n";
		
		return frase;
	}
	
}
