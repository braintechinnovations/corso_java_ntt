package com.lezione14.oopsemplice.esempio;

public class Main {

	public static void main(String[] args) {

		Studente giovanni = new Studente();
		giovanni.setNome("Giovanni");
		giovanni.setCognome("Pace");
		giovanni.setMatricola("123456");
		System.out.println(giovanni.stampaStudente());
		
		Studente mario = new Studente("Mario", "Rossi");
		System.out.println(mario.stampaStudente());
		mario.setMatricola("789456");
		System.out.println(mario.stampaStudente());
		
		Studente valeria = new Studente("Valeria", "Viola", "123456456789");
		System.out.println(valeria.stampaStudente());
	}

}
