package com.lezione14.oopsemplice;

public class Main {

	public static void main(String[] args) {

//		Libro libro_1 = new Libro();
//		libro_1.titolo = "Il signore degli anelli";
//		libro_1.autore = "Tolkien";
//		libro_1.isbn = "123456-123456-123456";
//		libro_1.anno_pubblicazione = 1920;
//		
//		Libro libro_2 = new Libro();
//		libro_2.titolo = "Guerra e Pace";
//		libro_2.autore = "Tolstoj";
//		
//		System.out.println(libro_1.titolo);
//		System.out.println(libro_2.titolo);
		
		Libro libro_3 = new Libro();
		libro_3.setTitolo("Il signore degli anelli");
		libro_3.setAutore("Tolkien");
		libro_3.setISBN("123456-123456-123456");
		libro_3.setAnnoPubblicazione(1920);
//		libro_3.stampaLibro();

		Libro libro_4 = new Libro();
		libro_4.setTitolo("Il ritorno del re");
		libro_4.setAutore("Tolkien");
		libro_4.setISBN("987654-123456-123456");
		libro_4.setAnnoPubblicazione(1924);
		libro_4.setDedica("A mia moglie");
//		libro_4.stampaLibro();
		
		libro_3.stampaLibroEstesa();
		libro_4.stampaLibroEstesa();
		
		Libro libro_5 = new Libro("Il signore degli anelli","Tolkien","123456-123456-123456");
		libro_5.stampaLibro();
		
		libro_5 = null;
		
		Libro libro_6;
		libro_6 = new Libro();
	}

}
