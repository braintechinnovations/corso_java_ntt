package com.lezione18.hw.preventivi;

public class Main {

	public static void main(String[] args) {

		/*
		 * Un preventivo � caratterizzato da:

		- Data del preventivo
		- Codice del preventivo
		- Indirizzo del Fornitore (campo composto)
		- Indirizzo del Cliente (campo composto)
		- Elenco delle voci di spesa:
		    - Nome
		    - Codice
		    - Quantit�
		    - Prezzo
		    - Sconto
		    - IVA
		    - Totale (calcolato in automatico)
		*/
		
//		Preventivo prev_1 = new Preventivo("2020-01-01");
//		Preventivo prev_2 = new Preventivo("2020-01-02");
//		
////		System.out.println(Preventivo.contatore_preventivi);
////		System.out.println(prev_1.getCodice_prev());
////		System.out.println(prev_2.getCodice_prev());
//		
//		Indirizzo spe_1 = new Indirizzo("Via le mani dal naso", "Roma", "123456", "Roma");
//		Indirizzo fat_1 = new Indirizzo("Via le mani dagli occhi", "Milano", "987654", "MI");
//
//		prev_1.setSpedizione(spe_1);
//		prev_1.setFatturazione(fat_1);
//		
//		Voce acqua = new Voce(
//				"Acqua San Benedetto",
//				"ACQ123456",
//				0.25f,
//				12,
//				20,
//				22
//				);
//		Voce acqua_guizza = new Voce(
//				"Acqua Guizza",
//				"7894564687",
//				0.25f,
//				12,
//				20,
//				22
//				);
//		
//		prev_1.addVoceSpesa(acqua);
//		prev_1.addVoceSpesa(acqua_guizza);
		
		//Nuovo approccio con classe di appoggio
		GestionePreventivi prev_1 = new GestionePreventivi(
				"2020-01-01", 
				"Via le mani dal naso", 
				"Roma", 
				"00789", 
				"Roma", 
				"Via le mani dagli occhi", 
				"Milano", 
				"789456", 
				"MI");
		
		prev_1.aggiungiVoce(
				"Acqua Guizza",
				"7894564687",
				0.25f,
				12,
				20,
				22);
		
		GestionePreventivi prev_2 = new GestionePreventivi("2020-01-02");
		prev_2.aggiungiIndirizzo(
				"SPEDIZIONE",
				"Via le mani dal naso", 
				"Roma", 
				"00789", 
				"Roma");
		prev_2.aggiungiIndirizzo(
				"FATTURAZIONE",
				"Via le mani dagli occhi", 
				"Roma", 
				"00789", 
				"Roma");
	
		
		/*
		 * Creare una struttura di immagazinamento di un elenco Studenti
		 * cercando di nascondere la struttura di immagazzinamento dati.
		 * 
		 * Voglio in particolare che ci sia una funzione dedicata a:
		 * - inserimento
		 * - ricerca
		 * - eliminazione (per matricola)
		 * - stampa dell'elenco
		 * 
		 */
		
	}

}
