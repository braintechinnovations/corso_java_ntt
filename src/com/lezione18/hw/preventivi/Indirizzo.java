package com.lezione18.hw.preventivi;

public class Indirizzo {

	private String via;
	private String citta;
	private String cap;
	private String provincia;
	
	Indirizzo(){
		
	}
	
	Indirizzo(String var_via, String var_cit, String var_cap, String var_pro){
		this.via = var_via;
		this.citta = var_cit;
		this.cap = var_cap;
		this.provincia = var_pro;
	}
	
	public String getVia() {
		return via;
	}
	public void setVia(String via) {
		this.via = via;
	}
	public String getCitta() {
		return citta;
	}
	public void setCitta(String citta) {
		this.citta = citta;
	}
	public String getCap() {
		return cap;
	}
	public void setCap(String cap) {
		this.cap = cap;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	
}
