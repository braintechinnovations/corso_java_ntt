package com.lezione18.hw.preventivi;

public class GestionePreventivi {

	private Preventivo prev;
	
	/**
	 * Metodo di creazione rapida di un preventivo
	 * @param var_data_pre
	 * @param spe_via
	 * @param spe_cit
	 * @param spe_cap
	 * @param spe_pro
	 * @param fat_via
	 * @param fat_cit
	 * @param fat_cap
	 * @param fat_pro
	 */
	GestionePreventivi(
			String var_data_pre,
			String spe_via,
			String spe_cit,
			String spe_cap,
			String spe_pro,
			String fat_via,
			String fat_cit,
			String fat_cap,
			String fat_pro
			) {
		
		Indirizzo ind_spe = new Indirizzo(spe_via, spe_cit, spe_cap, spe_pro);
		Indirizzo ind_fat = new Indirizzo(fat_via, fat_cit, fat_cap, fat_pro);
		
		this.prev = new Preventivo(var_data_pre);
		this.prev.setSpedizione(ind_spe);
		this.prev.setFatturazione(ind_fat);	
	}
	
	/**
	 * Metodo di creazione del preventivo che necessita l'uso del "aggiungiIndirizzo" per effettuare
	 * il set di un nuovo indirizzo
	 * @param var_data_pre
	 */
	GestionePreventivi(String var_data_pre){
		this.prev = new Preventivo(var_data_pre);
	}
	
	/**
	 * Metodo per l'inserimento di una nuova voce
	 * @param nome
	 * @param codice
	 * @param quant
	 * @param prezzo
	 * @param sconto
	 * @param iva
	 */
	public void aggiungiVoce(String nome, String codice, float quant, float prezzo, float sconto, float iva) {
		Voce voce_temp = new Voce(nome, codice, quant, prezzo, sconto, iva);
		this.prev.addVoceSpesa(voce_temp);
	}
	
	/**
	 * Metodo per l'inserimento di un nuovo indirizzo
	 * @param tipo solo di tipologia SPEDIZIONE | FATTURAZIONE
	 * @param var_via
	 * @param var_cit
	 * @param var_cap
	 * @param var_pro
	 */
	public void aggiungiIndirizzo(
			String tipo, 
			String var_via,
			String var_cit,
			String var_cap,
			String var_pro) {
		
		Indirizzo indirizzo = new Indirizzo(var_via, var_cit, var_cap, var_pro);

		switch(tipo) {
		case "SPEDIZIONE": 
			this.prev.setSpedizione(indirizzo);
			break;
		case "FATTURAZIONE":
			this.prev.setFatturazione(indirizzo); 
			break;
		}
		
	}
	
}
