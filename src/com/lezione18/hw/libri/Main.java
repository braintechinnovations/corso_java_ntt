package com.lezione18.hw.libri;

public class Main {

	public static void main(String[] args) {

		/*
		 * Scrivere un programma che gestisca il contenuto di una biblioteca.
		 * Il contenuto � un Supporto che pu� essere catalogato come "Libro" o "Rivista"
		 * Ogni supporto avr� una matricola univoca gestita autonomamente dal sistema!!!!!!!!!!!!!!!!!!!!
		 * 
		 * Permettere le CRUD di Libro o Rivista
		 */
		
		//MATRICOLA: SUP-1, SUP-2 ....
		/*
		 * Libro: SUP-1
		 * Libro: SUP-2
		 * Rivista: SUP-3
		 * 
		 */
		
//		Libro lib_1 = new Libro("123456", "Titolo Uno", "Editore Uno", "Autore Uno");
//		Libro lib_2 = new Libro("123457", "Titolo Due", "Editore Due", "Autore Due");
//		Libro lib_3 = new Libro("123458", "Titolo Tre", "Editore Tre", "Autore Tre");
//		Rivista riv_1 = new Rivista("123459", "Rivista Uno", "Editore Uno", 412);
//		Rivista riv_2 = new Rivista("123460", "Rivista Due", "Editore Due", 413);
//		Libro lib_4 = new Libro("123461", "Titolo Quattro", "Editore Quattro", "Autore Quattro");
//
//		System.out.println(lib_1.toString());
//		System.out.println(lib_2.toString());
//		System.out.println(lib_3.toString());
//		System.out.println(riv_1.toString());
//		System.out.println(riv_2.toString());
//		System.out.println(lib_4.toString());
		
		Edicola centrale = new Edicola();
		
		centrale.creation("123456", "Titolo Uno", "Editore Uno", "Autore Uno");
		centrale.creation("123457", "Titolo Due", "Editore Due", "Autore Due");
		centrale.creation("123458", "Titolo Tre", "Editore Tre", "Autore Tre");
		centrale.creation("123459", "Rivista Uno", "Editore Uno", 412);
		centrale.creation("123460", "Rivista Due", "Editore Due", 413);
		centrale.creation("123461", "Titolo Quattro", "Editore Quattro", "Autore Quattro");

		centrale.read();
		
//		if(centrale.delete("SUP-3")) {
//			System.out.println("\nEliminazione effettuata\n");
//		}
//		else {
//			System.out.println("\nMatricola non trovata!\n");
//		}
		
		if(centrale.update("SUP-1", "ABC123456", "Titolo Modificato", "Editore Modificato", "Pirandello")) {
			System.out.println("\nEliminazione effettuata\n");
		}
		else {
			System.out.println("\nMatricola non trovata!\n");
		}
		
		centrale.read();
	}

}
