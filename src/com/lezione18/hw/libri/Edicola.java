package com.lezione18.hw.libri;

import java.util.ArrayList;

public class Edicola {

	private ArrayList<Supporto> magazzino = new ArrayList<Supporto>();
	
	Edicola(){
		
	}
	
	//CRUD: Creation, Read, Update, Delete
	
//	public void creationLibro(
//			String var_isbn, String var_titolo, String var_editore, String var_autore) {
//		
//		Libro temp_lib = new Libro(var_isbn, var_titolo, var_editore, var_autore);
//		magazzino.add(temp_lib);
//	}
//	public void creationRivista(
//			String var_isbn, String var_titolo, String var_editore, int var_numero) {
//		
//		Rivista temp_riv = new Rivista(var_isbn, var_titolo, var_editore, var_numero);
//		magazzino.add(temp_riv);
//	}
	
	//OVERLOAD DEL METODO CREATION
	public void creation(
			String var_isbn, String var_titolo, String var_editore, String var_autore) {
		
		Libro temp_lib = new Libro(var_isbn, var_titolo, var_editore, var_autore);
		magazzino.add(temp_lib);
	}
	public void creation(
			String var_isbn, String var_titolo, String var_editore, int var_numero) {
		
		Rivista temp_riv = new Rivista(var_isbn, var_titolo, var_editore, var_numero);
		magazzino.add(temp_riv);
	}
	
	public void read() {
		for(int i=0; i<magazzino.size(); i++) {
			Supporto temp = magazzino.get(i);
			System.out.println(temp.toString());
		}
	}
	
	public boolean delete(String var_matr) {
		for(int i=0; i<magazzino.size(); i++) {
			Supporto temp = magazzino.get(i);
			if(temp.getMatricola().equals(var_matr)) {
				magazzino.remove(i);
				return true;
			}
		}
		
		return false;
	}
	
//	public boolean update(
//			String var_matr, String var_isbn, String var_titolo, String var_editore, String var_autore) {
//		
//		for(int i=0; i<magazzino.size(); i++) {
//			
//			Supporto temp = magazzino.get(i);
//			if(temp.getMatricola().equals(var_matr)) {
//				temp.setTitolo(var_titolo);
//				temp.setIsbn(var_isbn);
//				temp.setEditore(var_editore);
//				
//				if(temp instanceof Libro) {
//					Libro lib_temp = (Libro)temp;
//					lib_temp.setAutore(var_autore);
//				}
//
//				return true;
//			}
//		}
//		
//		return false;
//	}
//	
//	public boolean update(
//			String var_matr, String var_isbn, String var_titolo, String var_editore, int var_numero) {
//		
//		for(int i=0; i<magazzino.size(); i++) {
//			
//			Supporto temp = magazzino.get(i);
//			if(temp.getMatricola().equals(var_matr)) {
//				temp.setTitolo(var_titolo);
//				temp.setIsbn(var_isbn);
//				temp.setEditore(var_editore);
//				
//				if(temp instanceof Rivista) {
//					Rivista riv_temp = (Rivista)temp;
//					riv_temp.setNumero(var_numero);
//				}
//
//				return true;
//			}
//		}
//		
//		return false;
//	}
	
	public boolean update(
			String var_matr, String var_isbn, String var_titolo, String var_editore, String var_autore_numero) {
		
		for(int i=0; i<magazzino.size(); i++) {
			
			Supporto temp = magazzino.get(i);
			if(temp.getMatricola().equals(var_matr)) {
				temp.setTitolo(var_titolo);
				temp.setIsbn(var_isbn);
				temp.setEditore(var_editore);
				
				if(temp instanceof Libro) {
					Libro lib_temp = (Libro)temp;
					lib_temp.setAutore(var_autore_numero);
				}
				if(temp instanceof Rivista) {
					Rivista riv_temp = (Rivista)temp;
					riv_temp.setNumero(Integer.parseInt(var_autore_numero));
				}
				
				return true;
			}
		}
		
		return false;
	}
	
	/*
	 * Creare un sistema di gestione carte all'interno di una banca
	 * Le carte possono differenziarsi in:
	 * - Carta di Credito
	 * - Carta di Debito
	 * 
	 * La Carta di Credito ha un attributo personalizzato che si chiama FIDO
	 * La Carta di Debito ha un attributo personalizzato che si chiama DEPOSITO
	 * 
	 * Voglio sapere, quante carte di credito e di debito ho nella mia banca SENZA
	 * utilizzare metodi/attributi statici!
	 */
	
}
