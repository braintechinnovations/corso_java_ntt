package com.lezione18.hw.libri;

public abstract class Supporto {
	
	public static int contatore_libri = 0;

	protected String isbn;
	protected String titolo;
	protected String editore;
	protected String matricola;
	
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getTitolo() {
		return titolo;
	}
	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}
	public String getEditore() {
		return editore;
	}
	public void setEditore(String editore) {
		this.editore = editore;
	}
	public String getMatricola() {
		return this.matricola;
	}
	
	@Override
	public String toString() {
		return "Supporto [isbn=" + isbn + ", titolo=" + titolo + ", editore=" + editore + ", matricola=" + matricola
				+ "]";
	}
		
}
