package com.lezione18.hiding.studenti;

import java.sql.Array;
import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {

		/*
		 * Creare una struttura di immagazinamento di un elenco Studenti
		 * cercando di nascondere la struttura di immagazzinamento dati.
		 * 
		 * Voglio in particolare che ci sia una funzione dedicata a:
		 * - inserimento
		 * - ricerca
		 * - eliminazione (per matricola)
		 * - stampa dell'elenco
		 * 
		 */
		
		GestioneStudenti gestore = new GestioneStudenti();
		gestore.inserisciStudente("Giovanni", "Pace", "1234567");
		gestore.inserisciStudente("Mario", "Rossi", "987654654");
		gestore.inserisciStudente("Valeria", "Verdi", "8789797978");
		
		//In un futuro non troppo remoto...
//		gestore.ricercaStudente("987654654");
		
		gestore.ricercaStudente("");
		
//		if(gestore.eliminaStudente("987654654")) {
//			System.out.println("Eliminazione effettuata con successo!");
//		}
//		else {
//			System.out.println("Errore di eliminazione!");
//		}
//		
//		gestore.ricercaStudente("");
		
		if(gestore.modificaStudente("Giggino", "Giggetto", "987654654")) {
			System.out.println("Modifica effettuata con successo!");
		}
		else {
			System.out.println("Errore di modifica!");
		}
		
		gestore.ricercaStudente("");
		
		/*
		 * Scrivere un programma che gestisca il contenuto di una biblioteca.
		 * Il contenuto � un Supporto che pu� essere catalogato come "Libro" o "Rivista"
		 * Ogni supporto avr� una matricola univoca gestita autonomamente dal sistema!!!!!!!!!!!!!!!!!!!!
		 * 
		 * Permettere le CRUD di Libro o Rivista
		 */
		
	}

}
