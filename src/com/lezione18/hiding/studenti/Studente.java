package com.lezione18.hiding.studenti;

public class Studente {

	//VERSIONE DIPENDENTE DALLA MATRICOLA
	
	private String nome;
	private String cognome;
	private String matricola;
	
	Studente(){
		
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getMatricola() {
		return matricola;
	}
	public void setMatricola(String matricola) {
		this.matricola = matricola;
	}
	
	public String dettaglioStudente() {
		return 	"Nome: " + this.nome + "\n" + 
				"Cognome: " + this.cognome + "\n" + 
				"Matricola: " + this.matricola + "\n-----------";
	}
	
	
}
