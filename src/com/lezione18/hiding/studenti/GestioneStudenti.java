package com.lezione18.hiding.studenti;

import java.util.ArrayList;

public class GestioneStudenti {

	//VERSIONE DIPENDENTE DALLA MATRICOLA
	
	private ArrayList<Studente> elenco_studenti = new ArrayList<Studente>();
	
	GestioneStudenti() {
		System.out.println("Ho appena creato il middleware di gestione studenti");
	}
	
	/**
	 * Funzione di inserimento di uno studente in elenco.
	 * @param var_nome	Nome dello studente
	 * @param var_cogn	Cognome dello studente
	 * @param var_matr	Matricola dello studente
	 */
	public void inserisciStudente(
			String var_nome,
			String var_cogn,
			String var_matr) {
		
		Studente stud_temp = new Studente();
		stud_temp.setNome(var_nome);
		stud_temp.setCognome(var_cogn);
		stud_temp.setMatricola(var_matr);
		
		elenco_studenti.add(stud_temp);
	}
	
	/**
	 * Funzione di ricerca di uno studente per matricola
	 * @param var_matr Matricola dello studente sotto forma di String
	 */
	public void ricercaStudente(String var_matr) {
		for(int i=0; i<elenco_studenti.size(); i++) {
			
			Studente temp = elenco_studenti.get(i);
			if(temp.getMatricola().equals(var_matr) || var_matr.isEmpty()) {
				System.out.println(temp.dettaglioStudente());
			}
			
		}
	}
	
	public boolean eliminaStudente(String var_matr) {
		boolean esito_eliminazione = false;
		
		if(var_matr.isEmpty()) {
			return esito_eliminazione;
		}
		
		for(int i=0; i<elenco_studenti.size(); i++) {
			
			Studente temp = elenco_studenti.get(i);
			if(temp.getMatricola().equals(var_matr)) {
				elenco_studenti.remove(i);
				esito_eliminazione = true;
			}
	
		}
		return esito_eliminazione;
	}
	
	public boolean modificaStudente(String var_nome, String var_cogn, String var_matr) {
		boolean esito_aggiornamento = false;
		
		if(var_matr.isEmpty()) {
			return esito_aggiornamento;
		}
		
		for(int i=0; i<elenco_studenti.size(); i++) {
			Studente temp = elenco_studenti.get(i);
			if(temp.getMatricola().equals(var_matr)) {
				temp.setNome(var_nome);
				temp.setCognome(var_cogn);
				
				esito_aggiornamento = true;
			}
		}
		
		return esito_aggiornamento;
	}
	
}
