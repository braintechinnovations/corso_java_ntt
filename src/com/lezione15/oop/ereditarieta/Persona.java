package com.lezione15.oop.ereditarieta;

public class Persona {

	protected String nominativo;
	protected String telefono;
	protected String email;
	private boolean maggiorenne;
	
	public void setNominativo(String var_nom) {
		this.nominativo = var_nom;
	}
	public void setTelefono(String var_tel) {
		this.telefono = var_tel;
	}
	public void setEmail(String var_ema) {
		this.email = var_ema;
	}
	private void setMaggiorenne(boolean var_mag) {
		this.maggiorenne = var_mag;
	}
	
	public String getNominativo() {
		return this.nominativo;
	}
	public String getTelefono() {
		return this.telefono;
	}
	public String getEmail() {
		return this.email;
	}
	private boolean getMaggiorenne() {
		return this.maggiorenne;
	}
	
	public void salvaEta(int var_eta) {
		if(var_eta >= 18) {
//			this.maggiorenne = true;
			this.setMaggiorenne(true);
		}
		else {
//			this.maggiorenne = false;
			this.setMaggiorenne(false);
		}
	}
	
	public void stampaEta() {
		if(this.getMaggiorenne()) {
			System.out.println(this.getNominativo() + "Sei maggiorenne");
		}
		else {
			System.out.println(this.getNominativo() + "Sei minorenne");
		}
	}
	
	public void stampa() {
		System.out.println(
				"Nominativo: " + this.nominativo + "\n" +
				"Telefono: " + this.telefono + "\n" +
				"Email: " + this.email + "\n"
				);
	}
}
