package com.lezione15.oop.ereditarieta.sup;

public class MoleCola extends Lattina {

	//Se ridichiaro l'attributo sono VINCOLATO a ridichiarare anche i metodi che lo utilizzano
	//private String colore = "verde";
	
	@Override
	public String getColore() {
//		return super.colore;
		return this.colore;
	}
	
	@Override
	public void stampaTipologia() {
		System.out.println("Sono una MoleCola");
	}
	
	public void stampa() {
		super.stampaTipologia();
	}
	
}
