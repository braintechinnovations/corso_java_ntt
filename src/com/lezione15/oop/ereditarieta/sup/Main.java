package com.lezione15.oop.ereditarieta.sup;

public class Main {

	public static void main(String[] args) {


		MoleCola mol_1 = new MoleCola();
		System.out.println(mol_1.getColore());
		
//		mol_1.stampa();
		
		MoleCola mol_2 = mol_1;
		System.out.println(mol_2.getColore());
		
		mol_1.setColore("Giallo");				//SE non specifico il set come override della classe genitore, cambio il colore alla lattina!!!
		System.out.println(mol_2.getColore());

	}
	
	/*
	 * Descrivere le tipologie di prodotto vendute in una
	 * concessionaria!
	 */

}
