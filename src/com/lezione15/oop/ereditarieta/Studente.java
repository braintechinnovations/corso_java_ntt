package com.lezione15.oop.ereditarieta;

public class Studente extends Persona{
	
	private String matricola;
	private String data_immatr;
	
	Studente(){
		
	}
	
	Studente(String var_matr, String var_data){
		this.matricola = var_matr;
		this.data_immatr = var_data;
	}
	
	Studente(String var_nomi, String var_tele, String var_emai, String var_matr, String var_data){
		this.nominativo = var_nomi;
		this.telefono = var_tele;
		this.email = var_emai;
		this.matricola = var_matr;
		this.data_immatr = var_data;
	}
	
	public void setMatricola(String var_matr) {
		this.matricola = var_matr;
	}
	public void setDataImmatr(String var_data) {
		this.data_immatr = var_data;
	}
	
	public String getMatricola() {
		return this.matricola;
	}
	public String getDataImmatr() {
		return this.data_immatr;
	}
	
	@Override
	public void stampa() {
		System.out.println(
				"Nominativo: " + this.nominativo + "\n" +
				"Telefono: " + this.telefono + "\n" +
				"Email: " + this.email + "\n" +
				"Matricola: " + this.matricola + "\n" +
				"Data Immatricolazione: " + this.data_immatr + "\n"
				);
	}
	
}
