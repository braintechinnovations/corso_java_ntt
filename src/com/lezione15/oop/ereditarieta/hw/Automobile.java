package com.lezione15.oop.ereditarieta.hw;

public class Automobile extends Prodotto{
	
	private String colore;
	private String telaio;
	
	Automobile(
			String var_col, 
			String var_tel, 
			String var_ide, 
			float var_pre){
		this.colore = var_col;
		this.telaio = var_tel;
		super.identificatore = var_ide;
		super.prezzo = var_pre;
	}
	
	public void setColore(String var_col) {
		this.colore = var_col;
	}
	public void setTelaio(String var_tel) {
		this.telaio = var_tel;
	}
	
	public String getColore() {
		return this.colore;
	}
	public String getTelaio() {
		return this.telaio;
	}
	
	
}
