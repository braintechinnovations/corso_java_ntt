package com.lezione15.oop.ereditarieta.hw;

public class Prodotto {

	protected String identificatore;
	protected float prezzo = 0.0f;
	
	public void setIdentificatore(String var_iden) {
		this.identificatore = var_iden;
	}
	public void setPrezzo(float var_prez) {
		this.prezzo = var_prez;
	}
	
}
