package com.lezione15.oop.ereditarieta;

public class Professore extends Persona {

	private String livello = "";
	
	Professore(){
		
	}
	
	Professore(
				String var_nom,
				String var_tel,
				String var_ema,
				String var_liv
			){
		super.nominativo = var_nom;
		super.email = var_ema;
		super.telefono = var_tel;
		this.livello = var_liv;
	}
	
	public void setLivello(String var_live) {
		this.livello = var_live;
	}
	
	public String getLivello() {
		return this.livello;
	}
	
	@Override
	public void stampa() {
		System.out.println(
				"Nominativo: " + this.nominativo + "\n" +
				"Telefono: " + this.telefono + "\n" +
				"Email: " + this.email + "\n" +
				"Livello: " + this.livello + "\n"
				);
	}
	
}
