package com.lezione17.lista_poly;

public class Libro extends Supporto{

	private String autore;
	
	public void setAutore(String var_autore) {
		this.autore = var_autore;
	}
	public String getAutore() {
		return this.autore;
	}
	
	@Override
	public void stampaDettagli() {
		System.out.println(super.isbn + " - " + super.titolo + " - " + this.autore);
	}
	
	/*Creare un sistema automatico di gestione preventivi:
    Un preventivo � un documento che viene consegnato al cliente prima di effettuare un lavoro,
     questo contiene una previsione di spesa con il dettaglio del lavoro svolto sotto forma di
     elenco.

    Un preventivo � caratterizzato da:
    - Data del preventivo
    - Codice del preventivo (RICERCA)
    - Indirizzo del Fornitore (campo composto)
    - Indirizzo del Cliente (campo composto)
    - Elenco delle voci di spesa:
        - Nome
        - Codice
        - Quantit�
        - Prezzo
        - Sconto
        - IVA
        - Totale (calcolato in automatico)
    */
	
}
