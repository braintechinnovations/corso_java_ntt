package com.lezione17.lista_poly;

public abstract class Supporto {

	protected String isbn;
	protected String titolo;
	
	public void setIsbn(String var_isbn) {
		this.isbn = var_isbn;
	}
	public void setTitolo(String var_titolo) {
		this.titolo = var_titolo;
	}
	
	public String getIsbn() {
		return this.isbn;
	}
	public String getTitolo() {
		return this.titolo;
	}
	
	public void stampaDettagli() {
		System.out.println(this.isbn + " - " + this.titolo);
	}
}
