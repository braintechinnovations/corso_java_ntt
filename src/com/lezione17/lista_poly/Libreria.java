package com.lezione17.lista_poly;

import java.util.ArrayList;

public class Libreria {

	private ArrayList<Supporto> array_supporti = new ArrayList<Supporto>();
	
	public void addSupporto(Supporto obj_supporto) {
		this.array_supporti.add(obj_supporto);
	}
	
//	public void stampaCatalogo() {
//		for(int i=0; i<this.array_supporti.size(); i++) {
//			Supporto sup_temp = this.array_supporti.get(i);
//			sup_temp.stampaDettagli();
//		}
//	}
	
	public void stampaCatalogoFE() {
		for(Supporto oggetto: array_supporti) {
			System.out.println(oggetto.getClass().getName());	//Prendo il nome della classe con annesso il Package
			oggetto.stampaDettagli();
			System.out.println("\n");
		}
	}
	
//	public ArrayList<Libro> getElencoLibri() {
//		//HARD!!!
//	}
	
}
