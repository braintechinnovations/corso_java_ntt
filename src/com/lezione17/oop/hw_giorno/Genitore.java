package com.lezione17.oop.hw_giorno;

import java.util.ArrayList;

public class Genitore extends Persona{

	private ArrayList<Figlio> elenco_figli = new ArrayList<Figlio>();
	
	Genitore(){
//		this.elenco_figli = new ArrayList<Figlio>();
	}
	Genitore(String var_nome, String var_cognome){
		super.nome = var_nome;
		super.cognome = var_cognome;
//		this.elenco_figli = new ArrayList<Figlio>();
	}
	
	public void addFiglio(Figlio obj_figlio) {
		this.elenco_figli.add(obj_figlio);
	}
	
	public void stampaElenco() {
		
		String output = "";
		for(int i=0; i<this.elenco_figli.size(); i++) {
			Figlio fig_temp = this.elenco_figli.get(i);
			output += fig_temp.getNome() + " " + fig_temp.getCognome() + "\n";
		}
		
		System.out.println(output);
		
	}
	
}