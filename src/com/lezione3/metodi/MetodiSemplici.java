package com.lezione3.metodi;

public class MetodiSemplici {
	
	
	


//	public static void dimmiCiao(String var_nome) {
//		System.out.println("CIAO " + var_nome);
//	}
	
	/**
	 * Funzione di somma di tipo VOID che non mi restiusce nulla al Main!
	 * @param var_a Valore 1
	 * @param var_b Valore 2
	 */
	public static void effettuaSomma(int var_a, int var_b) {			//Funzione di tipo VOID (non ha ritorno)
		int somma = var_a + var_b;
		System.out.println("La somma �: " + somma);
	}
	
	/**
	 * Funzione di moltiplicazione di tipo int che restituisce il valore nel main, attenzione, non c'� System.out.println!
	 * @param var_a
	 * @param var_b
	 * @return	Valore prodotto restituito dalla funzione
	 */
	public static int effettuaMoltiplicazione(int var_a, int var_b) {	//Funzione con ritorno INT
		int prodotto = var_a * var_b;
		return prodotto;												//restituisce il valore di tipo int a chi lo chiama!
	}
	
	public static void main(String[] args) {
		
//		dimmiCiao();
//		dimmiCiao();
//		dimmiCiao();
		
//		String nome = "Giovanni";
//		dimmiCiao(nome);
//		
//		nome = "Marco";
//		String ciccio = "Mario";
//		dimmiCiao(ciccio);
//		
//		dimmiCiao("Valeria");
		
		int a = 23;
		int b = 34;
//		effettuaSomma(a, b);
//		effettuaSomma(56, 89);
		
		int valore = effettuaMoltiplicazione(87, 12);
		System.out.println(valore);	
	}

}
