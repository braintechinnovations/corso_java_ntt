package com.lezione3.contenitoricomplessi;

import java.util.ArrayList;

public class TestPassaggioParametri {

	public static void aggiungiAContenitore(String val_da_aggiungere, ArrayList<String> ciccio) {
		if(!val_da_aggiungere.isEmpty()) {
			ciccio.add(val_da_aggiungere);
		}
	}
	
	public static void main(String[] args) {

		ArrayList<String> contenitore = new ArrayList<String>();
		
		aggiungiAContenitore("", contenitore);
		
		for(int i=0; i<contenitore.size(); i++) {
			System.out.println(contenitore.get(i));
		}
		
	}
	
	

}
