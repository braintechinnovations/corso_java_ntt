package com.lezione1.controllicomplessi;

import java.util.Scanner;

public class ControlliComplessi {

	public static void main(String[] args) {

//		String sigla = "AQ";
//		
//		if(sigla.equals("AQ")) {
//			System.out.println("L'Aquila");
//		}
//		else {
//			if(sigla.equals("BO")) {
//				System.out.println("Bologna");
//			}
//			else {
//				if(sigla.equals("MI")) {
//					System.out.println("Milano");
//				}
//			}
//		}
		
		/* ---------------------- */
		
//		if(sigla.equals("AQ")) {
//			System.out.println("L'Aquila");
//		} else if(sigla.equals("BO")) {
//			System.out.println("Bologna");
//		} else if(sigla.equals("MI")) {
//			System.out.println("Milano");
//		} else {
//			System.out.println("Non ho trovato la provincia");
//		}
		
		/* ---------------------- */

//		if(sigla.equals("AQ")) {				// O(1)
//			System.out.println("L'Aquila");		// O(1)
//		}
//		
//		if(sigla.equals("BO")) {				// O(1)
//			System.out.println("Bologna");		// O(1)
//		}
//		
//		if(sigla.equals("MI")) {				// O(1)
//			System.out.println("Milano");		// O(1)
//		}
		
		/* ----------------------- */
//		
//		sigla = "AQ";
//		
//		switch(sigla) {
//			case "AQ":
//				System.out.println("L'Aquila");
//				/* tante istruzioni */
//				break;
//			case "BO":
//				System.out.println("Bologna");
//				break;
//			case "MI":
//				System.out.println("Milano");
//				break;
//			default:
//				System.out.println("Non ho trovato la provincia");
//		}
		
		/* --------------------------- */
		
		/**
		 * Scrivere un piccolo programma che, dato in input il giorno della settimana (in numero), vi restituisca il nome
		 * - 1 = Luned�
		 * - 2 = Marted�
		 * ...
		 * 
		 * NON RISCRIVENDO LO SWITCH-CASE ;)
		 * CHALLENGE... e se volessimo cambiare il sistema di conversione dei giorni nello stile americano?
		 * - 1 = Domenica
		 * - 2 = Luned�
		 * - 3 = Marted�
		 * ...
		 * 
		 * CHALLENGE... Se volete, scrivere un programma che all'inizio vi chieda la tipologia di data
		 * da convertire (se italiana o americana) e poi effettuare la conversione.
		 */
		

        Scanner interceptor = new Scanner(System.in);
        
        System.out.println("Indicare la tipologia di data - ITA per italiana o USA per americana:");
        String lingua = interceptor.nextLine();
        
        System.out.println("Inserisci il giorno della settimana:");
        int giorno = Integer.parseInt(interceptor.nextLine());
        
        if(lingua == "ITA") {
        	giorno = giorno + 1;
	        if(giorno == 8) {
	        	giorno =  1;
	        }
        }
        
        String frase = "";
        
        switch (giorno) {
	        case 1:
	            frase += "Domenica";
	            break;
	        case 2:
	        	frase += "Luned�";
	            break;
	        case 3:
	        	frase += "Marted�";
	            break;
	        case 4:
	        	frase += "Mercoled�";
	            break;
	        case 5:
	        	frase += "Gioved�";
	            break;
	        case 6:
	        	frase += "Venerd�";
	            break;
	        case 7:
	        	frase += "Sabato";
	            break;
	    }
        
        System.out.println(frase);
        
        interceptor.close();
        
        /* ---------------------------------------- */
		
//		System.out.println("INIZIO");
//
//		
//		if(1 == 1) {
//			String frase = "Ciao, Giovanni!";
//			System.out.println(frase);
//		}
//		
//		System.out.println(frase);				//La variabile dichiarata nella IF non � accessibile dal porocesso genitore.
//      
        
	}

}
