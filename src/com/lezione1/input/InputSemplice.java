package com.lezione1.input;

import java.util.Scanner;

public class InputSemplice {

	public static void main(String[] args) {

		/*
		Scanner interceptor = new Scanner(System.in);		//Dichiario lo Scanner
		
		System.out.println("Dimmi qual'� il tuo nome: ");	
		String nome = interceptor.nextLine();				//Prendo l'input e lo metto in una variabile
		
		if(nome.trim().length() > 0) {
			System.out.println("Dimmi qual'� il tuo cognome: ");	
			String cognome = interceptor.nextLine();
			
			if(cognome.trim().length() > 0)
				System.out.println("Il tuo nominativo �: " + nome + " " + cognome);
			else
				System.out.println("Errore nel Cognome");
		}
		else {
			System.out.println("Errore nel Nome");
		}
		
		System.out.println("TERMINATO!");
					
		interceptor.close();	//chiudo l'interceptor   
		*/
		
		/**
		 * TODO: Creare un sistema di inserimento anagrafica studente che utilizzi lo Scanner
		 * Le informazioni da memorizzare sono:
		 * - Nome
		 * - Cognome
		 * - Matricola
		 * 
		 * Una volta inserite queste informazioni mi verr� restituita una stringa composta da: Nome, Cognome - #Matricola
		 * Esempio: Giovanni, Pace - #ABCDEFG
		 * 
		 * 
		 * ATTENZIONE:
		 * Tutti i campi sono obbligatori, se inserisco un campo vuoto il sistema mi restituisce un errore e la procedura
		 * viene terminata!
		 */
		
		
		/* ---------------------------------------------------------- */
		
		/*
		 * Immaginiamo di voler inserire il numero di anni ed il sistema
		 * ci dice se siamo maggiorenni o no!
		 */
		
//		Scanner interceptor = new Scanner(System.in);
//		
//		System.out.println("Dimmi quanti anni hai:");
//		String anni = interceptor.nextLine();
//		
//		int anni_convertiti = Integer.parseInt(anni);		//Operazione di CAST che converte la stringa in intero.
//		
//		if(anni_convertiti >= 18) {
//			System.out.println("Maggiorenne");
//		}
//		else {
//			System.out.println("Minorenne");
//		}
//		
//		interceptor.close();
//		
		
		/**
		 * TODO:
		 * Verificare la possibilit� di ingresso all'interno di un locale, verificare che:
		 * 
		 *  La temperatura sia compresa tra 35 e 42 gradi
		 *  La temperatura non superi i 37.5 gradi
		 *  Nel caso si possa entrare nel locale, inserire i propri dati:
		 *  - Nome
		 *  - Cognome
		 *  - Numero di telefono
		 *  
		 *  Nel caso uno di questi dati sia mancante, interrompere l'inserimento
		 *  Nel caso in cui la temperatura non rispetti i limiti imposti sopra, vietare l'ingresso con una frase:
		 *  "ERRORE, non puoi entrare!"
		 */
		
		Scanner interceptor = new Scanner(System.in);
		
		System.out.println("Inserisci la temperatura:");
//		float temperatura = interceptor.nextFloat();
		
		String temp = interceptor.nextLine();
		float temp_convertita = Float.parseFloat(temp);
		
		//...
		
	}

}




