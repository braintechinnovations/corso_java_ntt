package com.lezione1.stringhe;

public class Stringhe {

	public static void main(String[] args) {

		//System.out.println("Ciao sono Giovanni");
		
		/*
		String frase = "Ciao sono Giovanni";	//Stringa come variabile
		System.out.println(frase);
		*/
		
		//System.out.println("CIAO SONO " + "Giovanni");
		
		/*
		String nome = "Giovanni";
		String cognome = "Pace";
		
		//System.out.println(nome + ", " + cognome);
		
		String frase = nome + ", " + cognome;
		System.out.println(frase);
		*/
		
		/* -------------------------- */
		
		/*
		int a, b;
		a = 5;
		b = 8;
		
		//System.out.println(a + b);
		//System.out.println("Il risultato �: " + a + b);
		//System.out.println(a + b + " � il risultato");
		//System.out.println("Il risultato �: " + (a + b));
		
		//int somma = a + b + " � il risultato";	//NO BUENO!
		 */
		
		/*
		 * Giovanni Pace � 10 anni vecchio ed ha una temperatura corporea
		 * di 36.6 gradi.
		 * Mario Rossi � 20 anni vecchio ed ha una temperatura corporea
		 * di 35.8 gradi.
		 * 
		 * VARIABILI: nominativo - et� - temperatura
		 */
		
		/*
		String nominativo = "Giovanni Pace";
		int eta = 10;
		float temp = 36.6f;
		
		System.out.println(
				nominativo + " � " + eta +  
				" anni vecchio ed ha una temperatura corporea di " + temp + " gradi"
				);
		
		nominativo = "Mario Rossi";
		eta = 20;
		temp = 35.8f;
		
		System.out.println(
				nominativo + " � " + eta +  
				" anni vecchio ed ha una temperatura corporea di " + temp + " gradi"
				);
				*/
		
		/* ---- LUNGHEZZA STRINGHE ---- */
		/*
		String nome = "Giovanni";
		int lunghezza_stringa = nome.length();
		
		System.out.println(lunghezza_stringa);
		*/
		
		/* ---- ALTRE FUNZIONI ---- */
		/*
		String nome = "Giovanni";
		System.out.println(nome.toUpperCase());
		System.out.println(nome.toLowerCase());
		*/
		
		//Paragoni tra stringhe diverse ma con "contenuto" uguale
		/*
		String nome_1 = "Giovanni";
		String nome_2 = "giovAnni";
		
		nome_1 = nome_1.toLowerCase();
		nome_2 = nome_2.toLowerCase();
		
		if(nome_1.equals(nome_2)) {
			System.out.println("I nomi sono uguali!");
		}
		else {
			System.out.println("I nomi non sono uguali!");
		}
		*/
		
		//Ricerca nella stringa
		/*
		String frase = "Ciao sono Giovanni, mi piace la programmazione";
		
		if(frase.indexOf("Giovanni") >= 0) {
			System.out.println("Giovanni � presente nella frase!");
		}
		else {
			System.out.println("Giovanni non � presente nella frase");
		}
		*/
		
		/* ---- Caratteri speciali ---- */
//		String frase = "Ciao sono Giovanni e da buon \"abruzzese\" mi piacciono gli arrosticini!";
//		System.out.println(frase);
		
//		String frase = "C:\\users\\SVILUPPO";
//		System.out.println(frase);
		
//		String frase = "Ciao sono Giovanni,\nquesta � una frase che va a capo;";
//		System.out.println(frase);
		
//		String frase = " Ciao sono Giovanni ";
//		System.out.println(frase.trim());		//Rimuove gli spazi
		
		//Verifica che gli spazi vengono rimossi prima della verifica
//		String frase = "   ";
//		if(frase.trim().length() > 0) {
//			System.out.println("Frase piena");
//		}
//		else {
//			System.out.println("Frase vuota");
//		}
		
		//Concatenazione alternativa
//		String frase_1 = "Ciao sono ";
//		String frase_2 = "Giovanni";
//		
//		String frase_totale = frase_1.concat(frase_2);
//		System.out.println(frase_totale);
//		System.out.println(frase_1 + frase_2);
				
		String frase = "   ";
		if(frase.isEmpty()) {
			System.out.println("Frase piena");
		}
		else {
			System.out.println("Frase vuota");
		}
		
	}

}
