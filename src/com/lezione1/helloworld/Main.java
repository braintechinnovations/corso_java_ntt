package com.lezione1.helloworld;

public class Main {

	public static void main(String[] args) {

		//Questo frammento di codice stampa "Hello World" e va nella nuova riga
		System.out.println("Hello World!");
		
		//Questo frammento di codice stampa "Hello World" per due volte consecutive senza andare in una nuova riga
		System.out.print("Hello World!");
		System.out.println("Hello World!");	//Questo � un commento "IN LINEA"
		
		/*
		 * Tutto quello che scrivo qui dentro
		 * pu� essere paragonato a un tema
		 * che si espande come mi serve!
		 */

		/*
		System.out.println("Ciao Mondo 1!");
		System.out.println("Ciao Mondo 2!");
		//System.out.println("Ciao Mondo 3!");
		System.out.println("Ciao Mondo 4!");
		*/
		
	}

}
