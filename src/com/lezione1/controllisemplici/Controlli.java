package com.lezione1.controllisemplici;

public class Controlli {

	public static void main(String[] args) {

		/*
		if(verificarsi della condizione) {
			Effettua l'operazione prestabilita
		}
		else {
			altrimenti esegui quest'altra operazione
		}
		*/
		
		/*
		int eta = 19;
		
		if(eta >= 18)
		{
			System.out.println("Sei maggiorenne");
		}
		else {
			System.out.println("Sei minorenne");
		}
		*/
		
		/*
		int a = 0;
		a = a + 5;
		a = a + 5;
		a = a + 5;
		a = a + 5;
		System.out.println(a);
		*/
		
		/*
		int eta = 2000;
		
		if(eta > 0) {
			if(eta >= 18)
			{
				System.out.println("Sei maggiorenne");
			}
			else {
				System.out.println("Sei minorenne");
			}
		}
		else {
			System.out.println("ERRORE ;)");
		}
		*/
		
		/* ------ AND ------ */
		/*
		int eta = 2000;
		
		
		if(eta >= 0 && eta <= 150) 		//AND logico, solo se 1 ^ 1 = 1
		{
			System.out.println("Et� accettabile");
		}
		else {
			System.out.println("ERRORE");
		}
		
		//Stessa identica cosa di dire "al run-time"
		if(true && false)
		{
			System.out.println("Et� accettabile");
		}
		else {
			System.out.println("ERRORE");
		}
		*/
		
		/* ------ OR ------ */
		/*
		int eta = 2000;
		
		if(eta < 0 || eta > 150) 
		{
			System.out.println("ERRORE");
		}
		else {
			System.out.println("Et� accettabile");
		}
		

		//Stessa identica cosa di dire "al run-time"
		if(false || true) //risultato � TRUE 
		{
			System.out.println("ERRORE");
		}
		else {
			System.out.println("Et� accettabile");
		}
		*/

		/* ------ NOT ------ */
		//boolean varBooleana = true;		//Camel case
		
		//boolean var_booleana = true;
		//System.out.println(!var_booleana);
		
		/*
		boolean var_booleana = true;
		var_booleana = !var_booleana;
		System.out.println(var_booleana);
		*/
		
		int eta = 2000;
		
		if(!(eta < 0 || eta > 150)) 			//Trasformazione di OR a comportamento di AND
		{
			System.out.println("Et� accettabile");
		}
		else {
			System.out.println("ERRORE");
		}
		
	}

}
