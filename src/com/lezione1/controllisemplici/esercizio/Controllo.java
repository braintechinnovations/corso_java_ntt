package com.lezione1.controllisemplici.esercizio;

public class Controllo {

	public static void main(String[] args) {

		/*
		 * TODO: ESERCIZIO!
		 * Scrivere da zero una funzione che, data la temperatura corporea mi dice se posso entrare in un locale
		 * stando attenti all'input che non deve essere inferiore di 35� e superiore a 42�
		 * 
		 * La temperatura di entrata � di 37.5�
		 * 
		 * Possibili output sono: "Puoi entrare" | "Non puoi entrare"
		 */
		
		/*
		float temperatura = 36.5f;
		
		if(!(temperatura < 35 || temperatura > 42)) {
			if(!(temperatura >= 37.5f)) {
				System.out.println("Puoi entrare");
			}
			else {
				System.out.println("Non puoi entrare");
			}
		}
		else {
			System.out.println("ERRORE");
		}
		*/
		
		/*

		float temperatura = 42.5f;
		boolean puoi_entrare = true;
		
		if(temperatura < 32 || temperatura > 37.5) {
			puoi_entrare = false;
		}
		
		if(puoi_entrare) {
			System.out.println("Puoi entrare");
		}
		else {
			System.out.println("Non puoi entrare");
		}
		*/
		
		/*
		if((true || false) && true) {
			System.out.println("Ciao");
		}
		*/
		
		/*
		 * Verificate tramite variabili booleane e controlli IF senza ramo else la seguente tipologia di operazione
		 * Se hai un'et� >= 18 anni allora sei maggiorenne, altrimenti sei minorenne facendo attenzione ai casi particolari:
		 * et� inferiore a zero e maggiore di 120 anni.
		 */
		
		int eta = 19;
		boolean eta_valida = true;
		
		
		if(eta < 0 || eta > 120) {
			System.out.println("Errore di validit�");
			eta_valida = false;
		}
		
		if(eta_valida) {
			if(eta >= 18) {
				System.out.println("Sei Maggiorenne");
			}
			else {
				System.out.println("Sei Minorenne");
			}
		}
	}
	

}
