package com.lezione22.file;

public class HomeWork {

	public static void main(String[] args) {

		/**
		 * Creare un software di gestione scolastica per un istito elementare.
		 * La suddivisione delle aule segue la seguente struttura:
		 * 
		 * SEZIONE: Con lettera da A ad F
		 * CLASSI: Da 1 a 5
		 * Es: Giovanni Pace della 3B, Mario Rossi della 5F
		 * 
		 * Voglio che:
		 * - All'inserimento di un nuovo studente, i dettagli vengano salvati all'interno della corretta
		 * combinazione SEZIONE-CLASSE in un file che ha il nome della classe e directory relativa alla sezione.
		 * - All'inizio di ogni file, inserire una stringa con su scritto la classe e sezione di riferimento.
		 * Es: REGISTRO 3B
		 * 
		 * - NON PERMETTERE L'INSERIMENTO DI STUDENTI GI� PRESENTI NEL FILE!
		 * (no omonimie nella stessa combinazione SEZIONE-CLASSE)
		 */
		
		//SECONDO!
		
		/**
		 * CREARE UN METODO CHE MI RESTITUISCA IN OUTPUT (file txt) TUTTI GLI STUDENTI DI TUTTE LE CLASSI
		 * Rispettare la formattazione:
		 * 1. Mario Rossi - 5B
		 * 2. Giovanni Pace - 5B
		 * 3. Maria Verdi -3B
		 * 
		 * Alla fine del file creato, restituire il numero di studenti per:
		 * - Classe
		 * - Sezione
		 * - Classe-Sezione
		 * - Totale
		 * 
		 * HARD: E se volessi un Thread per ogni sezione?
		 * HARD OPEN POINT: Se volessi effettuare il SORTING per cognome di ogni studente?
		 */
		
	}

}
