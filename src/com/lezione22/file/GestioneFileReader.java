package com.lezione22.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class GestioneFileReader {

	public static void main(String[] args) {

		String nomeFile = "\\concessionaria.txt";
		String filePath = System.getProperty("user.home") + "\\Desktop\\FWFR" + nomeFile;
		
		File f = new File(filePath);
		if(f.exists() && f.isFile() && f.canRead()) {
			
			try {
				FileReader fr = new FileReader(f);
				
				//Metodo con Buffered Reader
				BufferedReader buff = new BufferedReader(fr);
				
				String var_temp;
				
				while((var_temp = buff.readLine()) != null) {
					System.out.println(var_temp);
				}
				
				//Metodo con lo scanner
//				Scanner scan = new Scanner(fr);
//				while(scan.hasNext()) {
//					System.out.println(scan.nextLine());
//				}
				
				fr.close();
				
			} catch (IOException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
			
		}
		
	}

}
