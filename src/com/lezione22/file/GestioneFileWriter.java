package com.lezione22.file;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class GestioneFileWriter {

	public static void main(String[] args) {

		//Creazione di un nuovo file
//		String nomeFile = "\\esempio.txt";
//		String filePath = System.getProperty("user.home") + "\\Desktop\\FWFR";
//		
//		File d = new File(filePath);
//		if(d.exists() && d.isDirectory() && d.canWrite()) {
//			
//			File nuovo_file = new File(filePath + nomeFile);
//			
//			try {
//				if(nuovo_file.createNewFile()) {
//					System.out.println("File creato con successo!");
//				}
//				else {
//					System.out.println("File gi� esistente!");
//				}
//				
//				FileWriter fw = new FileWriter(nuovo_file, true);
//				fw.write("CIAO\n\n");
//				fw.write("Giovanni");
//				fw.close();
//
//				
//			} catch (IOException e) {
//				System.out.println(e.getMessage());
//			}
//			
//		}
		
		/**
		 * Rappresentazione all'interno di un file TXT dell'elenco automobili presenti
		 * all'interno di un concessionario.
		 */
		
		String nomeFile = "\\concessionaria.txt";
		String filePath = System.getProperty("user.home") + "\\Desktop\\FWFR";
		
		String[] lista_auto = {"BMW", "FIAT", "AUDI", "TESLA"};
		
		File d = new File(filePath);
		if(d.exists() && d.isDirectory() && d.canWrite()) {
			File nuovo_file = new File(filePath + nomeFile);
			
			try {
				nuovo_file.createNewFile();
				FileWriter fw = new FileWriter(nuovo_file, true);
				
				for(int i=0; i<lista_auto.length; i++) {
					fw.write(lista_auto[i] + "\n");
				}
				
				fw.close();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		
	}

}
