package com.lezione22.file.hw;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class GestioneStudenti {

	private String basePath = System.getProperty("user.home") + "\\Desktop\\gestione_scolastica";
	private ArrayList<Studente> elenco_stud = new ArrayList<Studente>();
	private HashMap<String, Integer> contatori_hm = new HashMap<String, Integer>();
	
	GestioneStudenti() {
		
	}

	/**
	 * Ricerca studente per nome e cognome
	 * @param varNome
	 * @param varCognome
	 * @param varSezione
	 * @param varClasse
	 * @return	-2 se il file non esiste | -1 se c'� stata una exception | 0 se non ho trovato lo studente! | 1 se c'� un caso di omonimia 
	 */
	private int ricercaStudente(String varNome, String varCognome, String varSezione, int varClasse) {
		File f = new File(this.basePath + File.separator + varSezione + File.separator + varClasse + ".txt");
		
		if(!f.exists() || !f.isFile())
			return -2;
		
		try {
			Scanner scan = new Scanner(f);
			while(scan.hasNext()) {
				String temp = scan.nextLine();
				
				String[] nominativo = temp.split(",");
				
//				Studente stud_temp = new Studente(nominativo[0],nominativo[1],varSezione,varClasse);
				if(varNome.equalsIgnoreCase(nominativo[0]) && varCognome.equalsIgnoreCase(nominativo[1])) {
					return 1;
				}
			}
			scan.close();
			
			return 0;
			
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
			return -1;
		}
	}
	
	/**
	 * Funzione per l'inserimento di un nuovo studente all'interno del file relativo alla sua classe e nella directory
	 * della sezione.
	 * @param varNome
	 * @param varCognome
	 * @param varSezione
	 * @param varClasse
	 * @return boolean con l'esito di inserimento
	 */
	public boolean inserisciStudente(String varNome, String varCognome, String varSezione, int varClasse) {
		Studente temp = new Studente(varNome,varCognome,varSezione,varClasse);
		
		//Ritorno false se c'� un caso di omonimia o una exception nel file
		if(ricercaStudente(varNome, varCognome, varSezione, varClasse) == 1 || ricercaStudente(varNome, varCognome, varSezione, varClasse) == -1)
			return false;
		
		//Fase di creazione della dir sezione
		File d = new File(this.basePath);
		
		if(d.exists() && d.isDirectory()) {
			
			String pathSezione = this.basePath + File.separator + temp.getSezione();
			File dirSezione = new File(pathSezione);
			if(!dirSezione.exists())
				dirSezione.mkdir();
			
			String pathFileClasse = pathSezione + File.separator + temp.getClasse() + ".txt";
			File fileClasse = new File(pathFileClasse);
			
			if(fileClasse.exists() && fileClasse.isDirectory())		//Elimino una cartella che potrebbe chiamarsi come il file selezionato!
				fileClasse.delete();
			
			try {
				boolean nuovo_file = fileClasse.createNewFile();
				
				FileWriter fw = new FileWriter(fileClasse, true);
				if(nuovo_file)														//IF SENZA GRAFFE PER UNA SOLA OPERAZIONE
					fw.write("CLASSE " + temp.getSezione() + temp.getClasse() + "\n");
				
				fw.write(temp.stampaDettaglio());
				
				fw.close();
				return true;
			} catch (IOException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
				return false;
			}
		}
		
		return false;

	}
	
	public void scriviSuFile(String var_contenuto, String var_nomefile) {
		String nome_file = var_nomefile + ".txt";
		
		File d = new File(basePath);
		if(d.exists() && d.isDirectory() && d.canWrite()) {
			File f = new File(basePath + File.separator + nome_file);
			
//			f.delete();
//			
//			try {
//				if(f.createNewFile()) {
//					FileWriter fw = new FileWriter(f, false);
//					fw.write(var_contenuto);
//					fw.close();
//					
//					System.out.println("File creato con successo!");
//				}
//				else {
//					System.out.println("Mi dispiace, � esploso qualcosa!");
//				}
//			} catch (IOException e) {
//				e.printStackTrace();
//			}

			try {
				f.createNewFile();
				FileWriter fw = new FileWriter(f, false);
				fw.write(var_contenuto);
				fw.close();
				
				System.out.println("File creato con successo!");
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		else {
			System.out.println("Errore di creazione del file");
		}
	}
	
	private void stampaElencoTotale() {
		
		String stringa_totale = "";
				
		
		for(int i=0; i<this.elenco_stud.size(); i++) {
			Studente stud_temp = this.elenco_stud.get(i);
			stringa_totale += (i+1) + "." + stud_temp.stampaDaFile() + "\n";
		}
		
		stringa_totale += "TOTALE: " + elenco_stud.size() + "\n";
		
		for(String chiave: this.contatori_hm.keySet()) {
			stringa_totale += "Il numero di studenti in " + chiave + " � di: " + this.contatori_hm.get(chiave) + "\n";
		}
		
//		System.out.println(stringa_totale);
		this.scriviSuFile(stringa_totale, "riepilogo_generale");
		
	}
	
	private void leggiFile(File obj_file) {

		if(obj_file.exists() && obj_file.isFile()) {
			try {
				String sezione = "";
				int classe = 0;
				
				Scanner scan = new Scanner(obj_file);
				int contatore = 0;
				while(scan.hasNext()) {
					String riga_temp = scan.nextLine();
					
					if(riga_temp.contains("CLASSE ")) {
						String[] titolo_split = riga_temp.split("CLASSE ");
						sezione = String.valueOf(titolo_split[1].charAt(0));						//Prendo il carattere della sezione
						classe = Integer.parseInt(String.valueOf(titolo_split[1].charAt(1)));		//Prendo la classe convertendola in INT
						
//						this.contatori_hm.put(titolo_split[1], 0);
					}
					
					if(riga_temp.contains(",")) {
						String[] riga_splittata = riga_temp.split(",");
						
						if(!sezione.isEmpty() && classe != 0) {
							Studente stu_temp = new Studente(riga_splittata[0], riga_splittata[1], sezione, classe);
							elenco_stud.add(stu_temp);
							
//							int valore_aggiornato = this.contatori_hm.get(sezione + classe) + 1;			//sezione + classe = "5B"
//							this.contatori_hm.put(sezione + classe, valore_aggiornato);
						}

						contatore ++;
					}
				}
				
				this.contatori_hm.put(sezione + classe, contatore);
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	
	}
	
	public void esporta() {
		this.elenco_stud.clear();						//Svuoto l'elenco prima di riempirlo di nuovo
		File d = new File(basePath);
		
		if(d.exists() && d.isDirectory()) {
			
			File[] elenco_dir = d.listFiles();
			for(int i=0; i<elenco_dir.length; i++) {
				if(elenco_dir[i].isDirectory()) {		//Controllo che quella da scansionare ulteriormente sia una Dir.
					
					String path_temp = basePath + File.separator + elenco_dir[i].getName();
					File d_temp = new File(path_temp);
					File[] elenco_file = d_temp.listFiles();
					
					for(int k=0; k<elenco_file.length; k++) {
						this.leggiFile(elenco_file[k]);
					}
					
				}
			}
			
		}
		
		this.stampaElencoTotale();
	}
}
