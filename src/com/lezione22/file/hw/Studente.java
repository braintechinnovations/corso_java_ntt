package com.lezione22.file.hw;

public class Studente {

	private String nome;
	private String cognome;
	private String sezione;
	private int classe;
	
	Studente(){
		
	}
	
	Studente(String nome, String cognome, String sezione, int classe) {
		this.nome = nome;
		this.cognome = cognome;
		this.sezione = sezione;
		this.classe = classe;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getSezione() {
		return sezione;
	}
	public void setSezione(String sezione) {
		this.sezione = sezione;
	}
	public int getClasse() {
		return classe;
	}
	public void setClasse(int classe) {
		this.classe = classe;
	}
	
	public String stampaDettaglio() {
		return this.getNome() + "," + this.getCognome() + "\n";
	}
	
	public String stampaDaFile() {
		return this.getNome() + " " + this.getCognome() + " - " + this.getSezione() + this.getClasse();
	}
	
	
}
