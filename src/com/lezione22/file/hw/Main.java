package com.lezione22.file.hw;

import java.util.Scanner;

public class Main {

	public static final boolean debug_enabled = true;
	
	public static void main(String[] args) {

		GestioneStudenti gest_stud = new GestioneStudenti();
		
		if(!debug_enabled) {
			Scanner interceptor = new Scanner(System.in);
			
			boolean inserimento_abilitato = true;
			while(inserimento_abilitato) {
				
				System.out.println("Cosa vuoi fare?\nI - Inserisci\nQ - Quit");
				String input =  interceptor.nextLine();
				
				switch(input) {
				case "I":
					
					System.out.println("Dammi il nome");
					String nome =  interceptor.nextLine().trim();
					//TODO: Validazione
					System.out.println("Dammi il Cognome");
					String cognome =  interceptor.nextLine().trim();
	
					System.out.println("Dammi la Sezione");
					String sezione =  interceptor.nextLine().trim();
					
					System.out.println("Dammi il Classe");
					int classe =  Integer.parseInt(interceptor.nextLine().trim());
					
					if(gest_stud.inserisciStudente(nome, cognome, sezione, classe))
						System.out.println("Studente inserito");
					else
						System.out.println("Errore di inserimento");
					break;
				case "Q":
					inserimento_abilitato = !inserimento_abilitato;
					break;
				default:
					System.out.println("Comando non riconosciuto");
				}
				
			}
		}
		else {
			//Corpo del debug;
//			gest_stud.inserisciStudente("Giovanni", "Pace", "B", 5);
//			gest_stud.inserisciStudente("Mario", "Rossi", "B", 5);
//			gest_stud.inserisciStudente("Valeria", "Verdi", "B", 5);
//			gest_stud.inserisciStudente("Valeria", "Verdi", "C", 4);
//			gest_stud.inserisciStudente("Valeria", "Verdi", "C", 5);
			
//			System.out.println(gest_stud.inserisciStudente("Mariolino", "Rossi", "B", 5));
			
//			gest_stud.esporta();
			
//			gest_stud.scriviSuFile("CIao ciccio!", "saluta_andonio");
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
