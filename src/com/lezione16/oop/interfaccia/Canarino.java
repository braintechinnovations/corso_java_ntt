package com.lezione16.oop.interfaccia;

public class Canarino implements Animale{

	private int numer_zampe = 2;
	
	@Override
	public void verso() {
		System.out.println("CIP CIP");
	}
	
	@Override
	public void movimento() {
		System.out.println("Vola");
	}
	
	public void setNumeroZampe(int var_zampe) {
		this.numer_zampe = var_zampe;
	}
	
}
