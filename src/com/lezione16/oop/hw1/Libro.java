package com.lezione16.oop.hw1;

public class Libro {

	private String nome;
	private String autore;
	private Etichetta eti_rif;
	
	Libro(){

	}

	public void setNome(String nome) {
	    this.nome = nome;
	}
	public void setAutore(String autore) {
	    this.autore = autore;
	}
	
	public String getNome() {
	    return this.nome;
	}
	public String getAutore() {
	    return this.autore;
	}
	
	public void setEtichetta(Etichetta obj_etichetta) {
		this.eti_rif = obj_etichetta;
	}
	public Etichetta getEtichetta() {
		return this.eti_rif;
	}
}
