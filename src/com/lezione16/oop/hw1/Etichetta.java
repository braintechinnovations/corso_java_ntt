package com.lezione16.oop.hw1;

public class Etichetta {
	
	private String scaffale;
	private int numero;
	private String categoria;
	
	Etichetta(){

	}

	public void setScaffale(String scaffale) {
	    this.scaffale=scaffale;
	}
	public void setNumero(int numero) {
	    this.numero = numero;
	}
	public void setCategoria(String categoria) {
	    this.categoria = categoria;
	}
	public String getScaffale() {
	    return this.scaffale;
	}
	public int getNumero() {
	    return this.numero;
	}
	public String getCategoria() {
	    return this.categoria;
	}
	
}
