package com.lezione16.oop.polimorfismo;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {

		Ferrari fer_1 = new Ferrari();
		fer_1.coloreSelezionato();
		
		Ferrari fer_2 = new Enzo();
		fer_2.coloreSelezionato();
		
		Enzo enz_1 = new Enzo();
		SF sf_1 = new SF();
		
		ArrayList<Ferrari> elenco = new ArrayList<Ferrari>();
		elenco.add(enz_1);
		elenco.add(fer_1);
		elenco.add(fer_2);
		elenco.add(sf_1);
		
		
	}

}
