package com.lezione16.oop.associazione.onetoone;

public class Main {

	public static void main(String[] args) {

//		Persona per_1 = new Persona("Giovanni Pace");
//		CodiceFiscale cf_1 = new CodiceFiscale("7894567896", "2020-02-01");
//		
//		per_1.setCodiceFiscale(cf_1);
////		cf_1.setPersonaRif(per_1);
//		
//		per_1.stampaDettaglio();
		
		CodiceFiscale cf_2 = new CodiceFiscale("7894567896", "2020-02-01");
		Persona per_2 = new Persona("Mario Rossi", cf_2);
		
		per_2.stampaDettaglio();
		
		/*
		 * Creare un sistema che colleghi un libro ad una
		 * ed una sola etichetta!
		 * 
		 * E se volessi associare un'etichetta anche ad una
		 * rivista ed un fumetto?
		 */
	}

}
