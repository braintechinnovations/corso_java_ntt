package com.lezione16.oop.associazione.onetoone;

public class Persona {

	private String nominativo;
	private CodiceFiscale cod_fis;
	
	Persona(){
		
	}
	Persona(String var_nom){
		this.nominativo = var_nom;
	}
	Persona(String var_nom, CodiceFiscale obj_cfis){
		this.nominativo = var_nom;
		this.cod_fis = obj_cfis;
	}
	
	
	public void setPersona(String var_nom) {
		this.nominativo = var_nom;
	}
	public String getPersona() {
		return this.nominativo;
	}
	
	public void setCodiceFiscale(CodiceFiscale obj_codf) {
		this.cod_fis = obj_codf;
	}
	public CodiceFiscale getCodiceFiscale() {
		return this.cod_fis;
	}
	
	public void stampaPersona(){
		System.out.println("Nominativo: " + this.nominativo);
	}
	
	public void stampaDettaglio() {
		System.out.println("Nominativo: " + this.nominativo);
		System.out.println("C.F.: " + this.cod_fis.getCodiceFiscale());
		System.out.println("Scad.: " + this.cod_fis.getDataScadenza());
		
	}
	
}
