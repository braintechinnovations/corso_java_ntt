package com.lezione16.oop.statiche;

public class Contatori {
	
	public final String DOCENTE = "Giovanni";

	public static int contatore_studenti = 0;
	
	public static int faiSomma(int a, int b) {
		return a + b;
	}
	
	public String getDocente() {
		return this.DOCENTE;
	}
	
}
