package com.lezione16.oop.statiche;

public class Studente {

	private String nominativo;
	private String matricola;

//	private static int contatore = 0;
	
	Studente(){
//		this.contatore++;
		Contatori.contatore_studenti++;
	}
	
	Studente(String var_nom, String var_mat){
		this.nominativo = var_nom;
		this.matricola = var_mat;
//		this.contatore++;
		Contatori.contatore_studenti++;
	}
	
	public void setNominativo(String var_nom) {
		this.nominativo = var_nom;
	}
	public void setMatricola(String var_mat) {
		this.matricola = var_mat;
	}
	
	public String getNominativo() {
		return this.nominativo;
	}
	public String getMatricola() {
		return this.matricola;
	}
	
}
