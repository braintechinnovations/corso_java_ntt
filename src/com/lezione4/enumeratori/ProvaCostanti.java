package com.lezione4.enumeratori;

public class ProvaCostanti {

	public static final String DOCENTE = "Giovanni";	//Costante che non pu� essere modificata
	
	public static void stampaDocente() {
		System.out.println(DOCENTE);					//Print della costante a cui accede la funzione
	}
	
	public static void main(String[] args) {

		System.out.println(DOCENTE);
		stampaDocente();
		
	}

}
