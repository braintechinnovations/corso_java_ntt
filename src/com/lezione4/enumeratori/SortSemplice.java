package com.lezione4.enumeratori;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Scanner;

public class SortSemplice {

	public static final int DIMENSIONE = 10;
	
	public static void main(String[] args) {

//		int[] array_numeri = new int[DIMENSIONE];
//		array_numeri[0] = 34;
//		array_numeri[1] = 87;
//		array_numeri[2] = 2;
//		array_numeri[3] = 76;
//		array_numeri[4] = 90;
//		array_numeri[5] = 4;
//		array_numeri[6] = 5;
//		array_numeri[7] = 12;
//		array_numeri[8] = 65;
//		array_numeri[9] = 98;
//		
//		System.out.println(array_numeri);
		
//		System.out.println(Arrays.toString(array_numeri));
		
//		Arrays.sort(array_numeri);

//		System.out.println(Arrays.toString(array_numeri));
		
//		//Ordina l'array al contrario (reverse) senza l'utilizzo di librerie
//		int[] array_reverse = new int[array_numeri.length];
//
////		for(int i=0; i<array_numeri.length; i++) {
////			System.out.println(array_numeri[(array_numeri.length - 1) - i]);
////		}
//		
//		for(int i=(array_numeri.length - 1); i>=0; i--) {
//			array_reverse[(array_numeri.length - 1) - i] = array_numeri[i];
//		}
//		
//		System.out.println(Arrays.toString(array_numeri));
//		System.out.println(Arrays.toString(array_reverse));
		
		//INTERI COME TIPO COMPLESSO
//		Integer[] array_sorgente = new Integer[DIMENSIONE];
//		array_sorgente[0] = 34;
//		array_sorgente[1] = 87;
//		array_sorgente[2] = 2;
//		array_sorgente[3] = 76;
//		array_sorgente[4] = 90;
//		array_sorgente[5] = 4;
//		array_sorgente[6] = 5;
//		array_sorgente[7] = 12;
//		array_sorgente[8] = 65;
//		array_sorgente[9] = 98;
//		
//		Arrays.sort(array_sorgente);
//		System.out.println(Arrays.toString(array_sorgente));
//		
//		Arrays.sort(array_sorgente, Collections.reverseOrder());
//		System.out.println(Arrays.toString(array_sorgente));
		
		//ALTRE TIPOLOGIE DI DATO
//		String[] rubrica = {"Giovanni", "Maria", "Pietro", "Riccardo", "Alessandra", "Libera"};
//
//		System.out.println("UNSORTED");
//		System.out.println(Arrays.toString(rubrica));
//
//		System.out.println("SORTED ASC");
//		Arrays.sort(rubrica);
//		System.out.println(Arrays.toString(rubrica));
//
//		System.out.println("SORTED DESC");
//		Arrays.sort(rubrica, Collections.reverseOrder());
//		System.out.println(Arrays.toString(rubrica));
	
		/*
		 * Scrivere un sistema che:
		 * - Alla selezione dell'opzione INSERISCI, dato in input un nome lo aggiunga alla rubrica.
		 * - Alla selezione dell'opzione STAMPA, venga richiesto se stampare la rubrica in maniera ASC o DESC ed effettua
		 * il relativo Output.
		 */

		Scanner interceptor =  new Scanner(System.in);
		
		boolean inserimento_ok = true;
		ArrayList<String> rubrica = new ArrayList<String>();
		
		while(inserimento_ok) {
			
			System.out.println("Seleziona il da farsi:\nQ - QUIT\nI - INSERIMENTO\nS - STAMPA");
			String input = interceptor.nextLine();
			
			switch(input) {
				case "Q":
					inserimento_ok = false;
					break;
				case "I":
					System.out.println("Inserisci il nome: ");
					String nome_inserito = interceptor.nextLine();
					
					if(!nome_inserito.isEmpty() && !verificaEsistenza(rubrica, nome_inserito)) {
						rubrica.add(nome_inserito);
					}
					else {
						System.out.println("Errore, il campo nome � vuoto o gi� presente in rubrica!");
					}
					break;
					
				case "S":
					System.out.println("In che ordine vuoi effettuare la stampa?\nASC - Ascendente\nDESC - Discendente");
					String val_ordinamento = interceptor.nextLine();

					ordinaArrayList(rubrica, val_ordinamento);
					stampaArrayList(rubrica);
					
					break;
			}
		}
	}
	
	/**
	 * Funzione per la verifica della presenza di un nome in rubrica.
	 * @param var_rubrica	ArrayList<String> con i nomi dei contatti
	 * @param var_nome	Nome da ricercare
	 * @return	true se il nome esiste, false altrimenti.
	 */
	public static boolean verificaEsistenza(ArrayList<String> var_rubrica, String var_nome) {
		if(Collections.frequency(var_rubrica, var_nome) > 0) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Funzione di stampa dell'array
	 * @param var_rubrica	ArrayList<String> con i nomi dei contatti
	 */
	public static void stampaArrayList(ArrayList<String> var_rubrica) {
		for(int i=0; i<var_rubrica.size(); i++) {
			System.out.println(var_rubrica.get(i));
		}
	}
	
	/**
	 * Funzione di ordinamento
	 * @param var_rubrica	ArrayList<String> con i nomi dei contatti
	 * @param var_tipo		Due possibili opzioni, DESC||ASC
	 */
	public static void ordinaArrayList(ArrayList<String> var_rubrica, String var_tipo) {
		switch(var_tipo) {
			case "ASC":
				Collections.sort(var_rubrica);
				break;
			case "DESC":
				Collections.sort(var_rubrica, Collections.reverseOrder());
				break;
		}
	}

}
