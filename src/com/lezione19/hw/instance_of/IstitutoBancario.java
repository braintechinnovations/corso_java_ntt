package com.lezione19.hw.instance_of;

import java.util.ArrayList;

public class IstitutoBancario {

	private String nome_istituto;
	private ArrayList<SupportoMagnetico> elenco_carte = new ArrayList<SupportoMagnetico>();
	
	IstitutoBancario(String var_nome) {
		this.nome_istituto = var_nome;
	}
	
	/**
	 * Funzione di inserimento di una nuova carta di credito
	 * @param tipo			CREDITO | DEBITO
	 * @param var_codice
	 * @param var_intes
	 * @param var_fid_dep
	 */
	public boolean insert(String tipo, String var_codice, String var_intes, float var_fid_dep) {
		switch(tipo) {
			case "CREDITO":
				Credito carta_cred = new Credito(var_codice, var_intes, var_fid_dep);
				elenco_carte.add(carta_cred);
				return true;
			case "DEBITO":
				Debito carta_debi = new Debito(var_codice, var_intes, var_fid_dep);
				elenco_carte.add(carta_debi);
				return true;
			default:
				return false;
		}
	}
	
	/**
	 * Funzione di conteggio delle carte di credito
	 * @param tipo	CREDITO | DEBITO | ""
	 * @return
	 */
	public int contaCarte(String tipo) {
		int contatore = 0;
		
		for(int i=0; i<elenco_carte.size(); i++) {
			SupportoMagnetico sup_temp = elenco_carte.get(i);
			
			switch(tipo) {
				case "CREDITO":
					if(sup_temp instanceof Credito) {
						contatore++;
					}
					break;
				case "DEBITO":
					if(sup_temp instanceof Debito) {
						contatore++;
					}
					break;
				default:
					contatore++;
			}
		}
		
		return contatore;
		
	}
	
}
