package com.lezione19.hw.instance_of;

public abstract class SupportoMagnetico {

	protected String codice;
	protected String intestatario;
	
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public String getIntestatario() {
		return intestatario;
	}
	public void setIntestatario(String intestatario) {
		this.intestatario = intestatario;
	}

}
