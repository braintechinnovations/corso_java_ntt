package com.lezione19.composizione_da_aggregazione;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {

		Fiore margherita = new Fiore("Margherita");
		Fiore rosa = new Fiore("Rosa");
		Fiore calendula = new Fiore("Calendula");
		
		ArrayList<Fiore> contenitore_provvisorio = new ArrayList<Fiore>();
		contenitore_provvisorio.add(margherita);
		contenitore_provvisorio.add(calendula);
		contenitore_provvisorio.add(rosa);
		
		MazzoDiFiori mazzolin = new MazzoDiFiori(contenitore_provvisorio);
		mazzolin.stampaFiori();
		
	}

}
