package com.lezione19.composizione_da_aggregazione;

import java.util.ArrayList;

public class MazzoDiFiori {

	private ArrayList<Fiore> mazzo;
	
	MazzoDiFiori(ArrayList<Fiore> arr_fiori) {
		this.mazzo = arr_fiori;
	}
	
	public void addFiore(Fiore obj_fiore) {
		mazzo.add(obj_fiore);
	}
	
	public int contatoreFiori() {
		return this.mazzo.size();
	}
	
	public void stampaFiori() {
		for(int i=0; i<this.mazzo.size(); i++) {
			System.out.println(this.mazzo.get(i).getNome());
		}
	}
	
}
