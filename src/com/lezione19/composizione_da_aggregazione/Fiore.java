package com.lezione19.composizione_da_aggregazione;

public class Fiore {
	
	private String nome;
	
	public Fiore(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
